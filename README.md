# flatsite

A simple HTML site to discover some awesome stuff !

## HOW TO guide

### Goals

Create a simple website with basic HTML and CSS.
And see how you can make your website portable, and how to deploy your application to the web easily.
Also you will see how we can automize the deployment process.

### Steps

**1. Clone the repository:**

```
git clone https://gitlab.com/polycloud_lab/flatsite.git <NAME_OF_YOUR_WEBSITE>
```

Open your new repository in your favorite IDE.

**2. Customize some HTML and CSS if wanted.**

Open the `index.html` file and try to edit some field to customize your website.

_if you are stuck at this point, use this magic command:_

```
git reset --hard step-1-website
```

**3. Make a Dockerfile to containerize the website.**

Now you want to make your application portable.
You will need to containerize your website. You can look at [this](https://www.docker.com/blog/how-to-use-the-official-nginx-docker-image/) guide to do it.

_if you are stuck at this point, use this magic command:_

```
git reset --hard step-2-container
```

**4. Make a GitLab-ci to deploy the app on CleverCloud.**

You now want to deploy the app on [CleverCloud](https://www.clever-cloud.com/fr/) (an awesome french cloud provider).
To automize this step, you want to deploy your application using [gitlab-ci](https://www.youtube.com/watch?v=ljth1Q5oJoo).

_if you are stuck at this point, use this magic command:_

```
git reset --hard step-3-gitlab-ci
```
